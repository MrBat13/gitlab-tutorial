# How to:

 1. creating a personal repository with the correct .gitignore and simple README.MD.
    - Navigate to this page to create your repository
    - Sample GitLab Project will create a simple README.md

![Creating GitLab repo](images/GitLab-creation.png)

- Press on the '+' button to add a file

![HomePage](images/GitLab-homepage.png)

- For the purpose of the excercise let's add a C .gitignore file
   
![HomePage](images/GitLab-ignore.png)

 2. creating develop and master branches.
    - Your default branch is master
    - Create a new branch called  'develop' by pressing the same red button and selecting 'New branch'

![HomePage](images/GitLab-develop.png)

3. setting the develop branch as the default.

![HomePage](images/GitLab-switch-default.png)

4. creating an issue for creating the current manual.

![Creating GitLab repo](images/GitLab-issue.png)
![Creating GitLab repo](images/GitLab-issue2.png)
    
- Fill the details as desired

5. creating a branch for the issue.

![Creating GitLab repo](images/GitLab-issue-branch.png)

 - select create branch from the dropdown menu

6. creating a merge request on the develop branch.

![Creating GitLab repo](images/GitLab-issue-merge.png)

 - select create merge request

7. commenting and accepting the request.

![Creating GitLab repo](images/GitLab-issue-request.png)

 - On the same page go to changes and add a commit with files to add
 - Accept merge

![Creating GitLab repo](images/GitLab-merge-approve.png)

8. creating a stable version in the master with a tag.

  ![Creating GitLab repo](images/GitLab-tag.png)

  ![Creating GitLab repo](images/Git-tag-apply.png)

9. working with wiki for the project.    

  repo at https://gitlab.com/MrBat13/gitlab-tutorial
